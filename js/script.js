function cleanCode(code){
    code = code.replace(/(\r\n|\n|\r)/gm, "");
    return code.trim()
  }
  
function getTemplate(app_key, hmac_key){
    return `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="true"/>
    <title>On-demand exports</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/dist/myscript.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/examples/examples.css">
    <style>
      #export-result {
        height: 100px;
        padding: 12px;
        overflow: auto;
      }
    </style>
    <script src="https://code.jquery.com/pep/0.4.3/pep.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/dist/myscript.min.js"></script>
  </head>
  <body>
    <div>
      <nav>
        <div class="button-div">
          <button id="clear" class="nav-btn btn-fab-mini btn-lightBlue" disabled>
            <img src="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/examples/assets/img/clear.svg">
          </button>
          <button id="undo" class="nav-btn btn-fab-mini btn-lightBlue" disabled>
            <img src="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/examples/assets/img/undo.svg">
          </button>
          <button id="redo" class="nav-btn btn-fab-mini btn-lightBlue" disabled>
            <img src="https://cdn.jsdelivr.net/gh/MyScript/MyScriptJS@master/examples/assets/img/redo.svg">
          </button>
        </div>
        <div class="spacer"></div>
        <button class="classic-btn" id="exportContent" disabled>Export</button>
      </nav>
      <div id="editor" touch-action="none"></div>
    </div>
    <script>
      var editorElement = document.getElementById("editor");
      var resultElement = document.getElementById("export-result");
      var undoElement = document.getElementById("undo");
      var redoElement = document.getElementById("redo");
      var clearElement = document.getElementById("clear");
      var exportElement = document.getElementById("exportContent");

      editorElement.addEventListener("changed", function (event) {
        undoElement.disabled = !event.detail.canUndo;
        redoElement.disabled = !event.detail.canRedo;
        exportElement.disabled = !event.detail.canExport;
        clearElement.disabled = event.detail.isEmpty;
      });

      editorElement.addEventListener("exported", function (evt) {
        if (evt.detail) {
            var ta = parent.document.getElementsByTagName("textarea")[0];  
            var startPos = ta.selectionStart;
            var endPos = ta.selectionEnd;
            ta.value = ta.value.substring(0, startPos)
                + evt.detail["exports"]["text/plain"]
                + ta.value.substring(endPos, ta.value.length);

            var frame = parent.document.getElementById("myscript");
            frame.parentNode.removeChild(frame);
        } else {
          resultElement.innerHTML = "";
        }
      });
      undoElement.addEventListener("click", function () {
        editorElement.editor.undo();
      });
      redoElement.addEventListener("click", function () {
        editorElement.editor.redo();
      });
      clearElement.addEventListener("click", function () {
        editorElement.editor.clear();
      });
      exportElement.addEventListener("click", function () {
        exportElement.disabled = true;
        editorElement.editor.export_();
      });

      MyScript.register(editorElement, {
        triggers: {
          exportContent: "DEMAND"
        },
        recognitionParams: {
          server: {
            scheme: "https",
            host: "webdemoapi.myscript.com",
            applicationKey: "${app_key}",
            hmacKey: "${hmac_key}",
            websocket: {
              pingEnabled: false,
              autoReconnect: true
            }
          }
        }
      });
      window.addEventListener("resize", function () {
        editorElement.editor.resize();
      });
    </script>
  </body>
</html>
`
}

function generateBookmarklet (){
    var app_key = document.getElementById('app_key').value;
    var hmac_key = document.getElementById('hmac_key').value;
    var html = getTemplate(app_key, hmac_key);
    code = `
var html = '${html}';
var iframe = document.createElement('iframe');
iframe.id = 'myscript';
iframe.style = "position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999; background-color: white;";
document.body.appendChild(iframe);
iframe.innerHTML = html;
var doc = document.getElementById("myscript").contentWindow.document;
doc.open();
doc.write(html);
doc.close();   
    `
    bookmarklet = cleanCode(code);
    var output = "javascript:" +  encodeURIComponent("(function(){" + bookmarklet  +  "})();");
    var link = document.getElementById("bookmarklet-a")
    link.href = output;
    link.text = "Handwriting with OLAT";
}